from django.contrib import admin
from .models import Feature, SubFeature, Rate, ClientCalculationResult, Technologies

# Register your models here.
admin.site.register(Feature)
admin.site.register(SubFeature)
admin.site.register(Rate)
admin.site.register(ClientCalculationResult)
admin.site.register(Technologies)
