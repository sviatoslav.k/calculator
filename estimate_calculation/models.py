from django.db import models


# Create your models here.

class Feature(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=180)
    backend_min = models.IntegerField()
    backend_max = models.IntegerField()
    frontend_min = models.IntegerField()
    frontend_max = models.IntegerField()
    IOS_min = models.IntegerField()
    IOS_max = models.IntegerField()
    android_min = models.IntegerField()
    android_max = models.IntegerField()
    cross_platform_min = models.IntegerField()
    cross_platform_max = models.IntegerField()

    def __str__(self):
        return self.name


class SubFeature(models.Model):
    name = models.CharField(max_length=150)
    feature = models.ForeignKey(Feature, on_delete=models.CASCADE, related_name='sub_features')

    def __str__(self):
        return self.name


class Rate(models.Model):
    type_of_developing = models.CharField(primary_key=True, max_length=125)
    price = models.IntegerField()

    def __str__(self):
        return f"{self.type_of_developing}  -> rate {self.price} $"


class ClientCalculationResult(models.Model):
    name = models.CharField(max_length=120)
    email_address = models.EmailField()
    link_for_spreadsheet = models.CharField(max_length=10000)

    def __str__(self):
        return self.email_address


class TypesOfDevelopment(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class Technologies(models.Model):
    name_of_technology = models.CharField(max_length=150)
    types_of_development = models.ManyToManyField(TypesOfDevelopment)

    def __str__(self):
        return self.name_of_technology
