from django.shortcuts import get_list_or_404

from django.conf import settings
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from googleapiclient.discovery import build
from .serializers import RateSerializer
from .models import Feature, Rate, ClientCalculationResult, TypesOfDevelopment
import math

HOURS_PER_DAY = 8

DEFAULT_TEAM_SIZE = 1
DEFAULT_RATE_IF_DB_IS_EMPTY = 20


def bigest_time_for_one_platform(data):
    if data:
        max_key = max(data, key=data.get)
    else:
        max_key = None
    return data[max_key]


def add_cybearsoft_to_feature_name(data):
    return [{**x, 'name': f'{x["name"]}(CybearSoft)'} for x in data]


def insert_data_into_spreadsheet(cybear_soft_results, client_results, client_selected_and_estimated_dicts,
                                 cybear_soft_features_estimation_dicts,
                                 id_of_google_sheet, client_rates, client_technologies):
    cybear_soft_results['total_spend_hours']['name'] = 'total(CybearSoft)'
    client_results['total_spend_hours']['name'] = 'onsite_development'

    SPREADSHEET_ID = id_of_google_sheet

    credentials = ServiceAccountCredentials.from_json_keyfile_name(settings.GOOGLE_SHEET_CREDENTIALS_JSON,
                                                                   settings.GOOGLE_SHEET_SCOPES)

    service = build('sheets', 'v4', credentials=credentials)

    data = concat_results(cybear_soft_features_estimation_dicts, client_selected_and_estimated_dicts,
                          )

    values_to_insert = [
        ["Onsite developer's rate"] + [value for value in client_rates.values() for _ in range(2)],
        ["Onsite developer's technology"] + [value for value in client_technologies.values() for _ in range(2)]]

    request = service.spreadsheets().values().append(
        spreadsheetId=SPREADSHEET_ID,
        range='Sheet1',
        valueInputOption='RAW',
        insertDataOption='INSERT_ROWS',
        body={'values': values_to_insert}
    )
    response = request.execute()

    data = data + [cybear_soft_results['total_spend_hours']] + [client_results['total_spend_hours']]

    values_to_insert = add_to_sheet(data)
    request = service.spreadsheets().values().append(
        spreadsheetId=SPREADSHEET_ID,
        range='Sheet1',
        valueInputOption='RAW',
        insertDataOption='INSERT_ROWS',
        body={'values': values_to_insert}
    )
    response = request.execute()

    values_to_insert = [["", ""], ["", ""], ["name", "value"],
                        ["CybearSoft estimated cost numeric display",
                         f"{cybear_soft_results['price']['min']}-{cybear_soft_results['price']['max']} usd"],
                        ["CybearSoft estimated max amount of days",
                         cybear_soft_results['amount_of_days_for_development']],
                        ["CybearSoft amount of developers", cybear_soft_results['team_size']],
                        ["", ""],
                        ["Onsite estimated cost numeric display",
                         f"{client_results['price']['min']}-{client_results['price']['max']} usd"],
                        ["Onsite estimated max amount of days", client_results['amount_of_days_for_development']],
                        ["Onsite amount of developers", client_results['team_size']],

                        ]

    request = service.spreadsheets().values().append(
        spreadsheetId=SPREADSHEET_ID,
        range='Sheet1',
        valueInputOption='RAW',
        insertDataOption='INSERT_ROWS',
        body={'values': values_to_insert}
    )
    response = request.execute()


def calculate_statistic(data, rate):
    for feature in data:
        feature.pop('name')
        if "sub_features" in feature:
            feature.pop('sub_features')
        if 'id' in feature:
            feature.pop('id')
    all_feature_dict = sum_all_dicts(data)
    max_time_for_one_technology = bigest_time_for_one_platform(all_feature_dict)
    amount_of_days_for_development = math.ceil(max_time_for_one_technology / HOURS_PER_DAY / DEFAULT_TEAM_SIZE)
    mapping = {
        'backend_min': 'backend_rate', 'backend_max': 'backend_rate',
        'frontend_min': 'frontend_rate', 'frontend_max': 'frontend_rate',
        'IOS_min': 'iOS_rate', 'IOS_max': 'iOS_rate',
        'android_min': 'android_rate', 'android_max': 'android_rate',
        'cross_platform_min': 'cross_platform_rate', 'cross_platform_max': 'cross_platform_rate'
    }

    prices_across_technologies = {key: all_feature_dict[key] * rate[mapping[key]] for key in all_feature_dict}
    total_spend_hours = {key: all_feature_dict[key] for key in all_feature_dict}
    max_price = sum(value for key, value in prices_across_technologies.items() if key.endswith('_max'))
    min_price = sum(value for key, value in prices_across_technologies.items() if key.endswith('_min'))
    return {"price": {"min": min_price, "max": max_price},
            "amount_of_days_for_development": amount_of_days_for_development,
            "team_size": DEFAULT_TEAM_SIZE,
            "total_spend_hours": total_spend_hours}


def sum_all_dicts(list_of_dicts):
    sum_dict = {}
    for d in list_of_dicts:
        for key, value in d.items():
            if key in sum_dict:
                sum_dict[key] += value
            else:
                sum_dict[key] = value
    if sum_dict.get('sub_features'):
        sum_dict.pop('sub_features')
    return sum_dict


def get_features_by_ids(id_list):
    features = get_list_or_404(Feature, id__in=id_list)
    return features


def get_current_rate():
    queryset = Rate.objects.all()
    serializer = RateSerializer(queryset, many=True)
    return [dict(data) for data in serializer.data]


def create_google_sheet(client_name, ):
    credentials = ServiceAccountCredentials.from_json_keyfile_name(settings.GOOGLE_SHEET_CREDENTIALS_JSON,
                                                                   settings.GOOGLE_SHEET_SCOPES)
    client = gspread.authorize(credentials)

    spreadsheet = client.create(f'estimate comparison for {client_name}')
    spreadsheet_id = spreadsheet.id

    spreadsheet.share('', perm_type='anyone', role='writer')
    return f"https://docs.google.com/spreadsheets/d/{spreadsheet_id}", spreadsheet_id


def save_interested_user_data(client_name, client_email, link_for_spreadsheet):
    client_object = ClientCalculationResult(name=client_name, email_address=client_email,
                                            link_for_spreadsheet=link_for_spreadsheet)
    client_object.save()
    return True


def add_to_sheet(data):
    values_to_insert = [
        [
            'name',
            'backend_min', 'backend_max',
            'frontend_min', 'frontend_max',
            'IOS_min', 'IOS_max',
            'android_min', 'android_max',
            'cross_platform_min', 'cross_platform_max'
        ]
    ]
    for entry in data:
        values_to_insert.append([
            entry['name'],
            entry['backend_min'], entry['backend_max'],
            entry['frontend_min'], entry['frontend_max'],
            entry['IOS_min'], entry['IOS_max'],
            entry['android_min'], entry['android_max'],
            entry['cross_platform_min'], entry['cross_platform_max']
        ])
    return values_to_insert


def concat_results(
        client_selected_and_estimated_dicts,
        cybear_soft_features_estimation_dicts):
    concatenated_results = [val for pair in
                            zip(client_selected_and_estimated_dicts, cybear_soft_features_estimation_dicts) for val
                            in
                            pair]
    return concatenated_results


def is_default_rates_inserted():
    keys_for_check = ["backend_rate", "frontend_rate", "iOS_rate", "android_rate", 'cross_platform_rate']
    for rate_name in keys_for_check:
        try:
            rate = Rate.objects.get(type_of_developing=rate_name)
        except Rate.DoesNotExist:
            rate = Rate.objects.create(type_of_developing=rate_name, price=DEFAULT_RATE_IF_DB_IS_EMPTY)
    return True


def is_default_types_of_technologies_inserted():
    default_types_list = ['Backend',
                          'Frontend',
                          "IOS",
                          "Android",
                          "Cross_platform"]
    for type in default_types_list:
        try:
            answer = TypesOfDevelopment.objects.get(name=type)
        except:
            default_type = TypesOfDevelopment(name=type)
            default_type.save()

    return True


try:
    print('default_types_of_technologies_inserted')
    print('default_rates_inserted')
    is_default_types_of_technologies_inserted()
    is_default_rates_inserted()

except Exception:
    pass
