from django.urls import path, include
from .views import ListOfFeatures, RetrieveEstimationResults,ListOfTechnologies

urlpatterns = [
    path('list_of_features/', ListOfFeatures.as_view(), name='list-of-features'),
    path('list_of_technologies', ListOfTechnologies.as_view()),
    path('retrieve_results/', RetrieveEstimationResults.as_view()),

]
