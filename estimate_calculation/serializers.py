from rest_framework import serializers
from .models import Feature, SubFeature, Rate, ClientCalculationResult, Technologies, TypesOfDevelopment


class SubFeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubFeature
        fields = '__all__'


class FeatureSerializer(serializers.ModelSerializer):
    sub_features = SubFeatureSerializer(many=True, read_only=True)

    class Meta:
        model = Feature
        fields = '__all__'


class RateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rate
        fields = '__all__'


class ClientCalculationResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientCalculationResult
        fields = '__all__'


class TypesOfDevelopmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypesOfDevelopment
        fields = '__all__'


class TechnologiesSerializer(serializers.ModelSerializer):
    types_of_development = TypesOfDevelopmentSerializer(many=True)

    class Meta:
        model = Technologies
        fields = '__all__'


class CustomFeatureSerializer(serializers.Serializer):
    features = FeatureSerializer(many=True)
    client_email = serializers.EmailField()
    client_name = serializers.CharField()

    backend_rate = serializers.IntegerField()
    frontend_rate = serializers.IntegerField()
    iOS_rate = serializers.IntegerField()
    android_rate = serializers.IntegerField()
    cross_platform_rate = serializers.IntegerField()

    backend_technology = serializers.CharField(default=None)
    frontend_technology = serializers.CharField(default=None)
    iOS_technology = serializers.CharField(default=None)
    android_technology = serializers.CharField(default=None)
    cross_platform_technology = serializers.CharField(default=None)
