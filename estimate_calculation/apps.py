from django.apps import AppConfig


class EstimateCalculationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'estimate_calculation'
